package ru.t1.zkovalenko.tm.exception.user;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("You are not logged in. Please log in and try again");
    }

}
