package ru.t1.zkovalenko.tm.api.repository;

import ru.t1.zkovalenko.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    void clear(String userId);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    boolean existById(String userId, String id);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    int getSize(String userId);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    M add(String userId, M model);

    M remove(String userId, M model);

}
