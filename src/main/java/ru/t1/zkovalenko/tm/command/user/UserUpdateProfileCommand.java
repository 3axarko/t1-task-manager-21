package ru.t1.zkovalenko.tm.command.user;

import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String NAME = "user-update-profile";

    private final String DESCRIPTION = "User update profile";

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("First name");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Last name");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Middle name");
        final String middleName = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
