package ru.t1.zkovalenko.tm.exception.user;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public class AbstractUserException extends AbstractException {

    public AbstractUserException() {
        super();
    }

    public AbstractUserException(String message) {
        super(message);
    }

    public AbstractUserException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(Throwable cause) {
        super(cause);
    }

    public AbstractUserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
