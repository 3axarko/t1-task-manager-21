package ru.t1.zkovalenko.tm.exception.user;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Email not exists");
    }

}
