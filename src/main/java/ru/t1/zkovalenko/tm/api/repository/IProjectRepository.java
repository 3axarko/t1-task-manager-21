package ru.t1.zkovalenko.tm.api.repository;

import ru.t1.zkovalenko.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}
