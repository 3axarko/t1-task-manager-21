package ru.t1.zkovalenko.tm.api.service;

import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

}
