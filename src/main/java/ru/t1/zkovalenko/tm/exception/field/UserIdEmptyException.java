package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class UserIdEmptyException extends AbstractException {

    public UserIdEmptyException() {
        super("UserId is empty");
    }

}
