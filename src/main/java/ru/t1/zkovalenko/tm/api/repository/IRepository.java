package ru.t1.zkovalenko.tm.api.repository;

import ru.t1.zkovalenko.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    M add(M model);

    void clear();

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    boolean existById(String id);

    int getSize();

}
